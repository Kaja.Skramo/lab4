package datastructure;

import java.lang.reflect.Array;
import java.util.Arrays;

import cellular.CellState;

public class CellGrid implements IGrid {

    int rows;
    int cols;
    
    CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;

        grid = new CellState[rows][columns];

        for (int r=0; r<rows; r++) {
            for(int c=0; c<cols; c++) {
                grid[r][c] = initialState;
            }
        }

	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if(row <= 0 && row > numRows()) {
            throw new IndexOutOfBoundsException("Grids need at least 1 row, you supplied " + row);
        }
        if(column <= 0 && column > numRows()) {
            throw new IndexOutOfBoundsException("Grids need at least 1 row, you supplied " + column);
        }

        grid[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        if(row <= 0 && row > numRows()) {
            throw new IndexOutOfBoundsException("Grids need at least 1 row, you supplied " + row);
        }
        if(column <= 0 && column > numRows()) {
            throw new IndexOutOfBoundsException("Grids need at least 1 row, you supplied " + column);
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid gridCopy = new CellGrid(this.rows, this.cols, CellState.DEAD);

        for (int r=0; r<rows; r++) {
            for(int c=0; c<cols; c++) {
                gridCopy.set(r, c, grid[r][c]);
            }
        }

        return gridCopy;
    }
    
}
